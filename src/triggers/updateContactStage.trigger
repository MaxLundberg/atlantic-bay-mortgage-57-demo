trigger updateContactStage on Contact (before insert, before update) {
if(checkRecursive.runOnce())
    {
    for (Contact so : Trigger.new) {
    try {
        
    
        if(string.isNotBlank(so.Jungo_LenderApp__Loan_Officer__c)){
        //friends remind friends to bulkify
            if(so.Jungo_LenderApp__Stage__c.contains( 'Audited by Compliance') || so.Jungo_LenderApp__Stage__c.contains( 'Funded (ABMG)') || so.Jungo_LenderApp__Stage__c.contains( 'Funded by Corr') || so.Jungo_LenderApp__Stage__c.contains( 'Loan Sold') || so.Jungo_LenderApp__Stage__c.contains( 'Paid in Full') || so.Jungo_LenderApp__Stage__c.contains( 'Purchased') || so.Jungo_LenderApp__Stage__c.contains( 'Purchased from Corr')){
            so.Jungo_LenderApp__Stage__c = 'Closed Client';

        }
        else if(so.Jungo_LenderApp__Stage__c.contains('Approved with Conditions') || so.Jungo_LenderApp__Stage__c.contains('Clear to Close') || so.Jungo_LenderApp__Stage__c.contains('Compliance Held') || so.Jungo_LenderApp__Stage__c.contains('Cond W U/W') || so.Jungo_LenderApp__Stage__c.contains('Docs Sent') || so.Jungo_LenderApp__Stage__c.contains('In Underwriting') || so.Jungo_LenderApp__Stage__c.contains('Instruc Out') || so.Jungo_LenderApp__Stage__c.contains('New Loan (In Ops)') || so.Jungo_LenderApp__Stage__c.contains('Not Approvable (As Is)') || so.Jungo_LenderApp__Stage__c.contains('Not Underwritable') || so.Jungo_LenderApp__Stage__c.contains('Resubmitted') || so.Jungo_LenderApp__Stage__c.contains('TBD Approved') || so.Jungo_LenderApp__Stage__c.contains('TBD Not Approvable (As Is)') || so.Jungo_LenderApp__Stage__c.contains('With Closing') ){
            so.Jungo_LenderApp__Stage__c = 'Loan in Process';
        }
        else if (so.Jungo_LenderApp__Stage__c.contains('[Not Assigned]') || so.Jungo_LenderApp__Stage__c.contains('Lead (Credit Not Pulled)')) {
        	so.Jungo_LenderApp__Stage__c = 'Lead';
        }
        else{
       // so.Jungo_LenderApp__Stage__c = 'No Status';
        }
    }
    } catch(Exception e) {
        System.debug(e.getMessage());
    }
    }
    }
}