/*********************************************************************
*
*   File Name: UpdateContact_BeforeInsert.cls
*
*   File Description: Trigger excutes on Before Insert of Contact 
*
**********************************************************************
*
*   Date             Author               Change
*   08/17/15         Rob                  Initial creation                        
*
*********************************************************************/
trigger UpdateContact_BeforeInsert on Contact (before insert, before update) 
{
    try {
    UpdateContactHelper.updateContact(trigger.new);
        } 
        catch(Exception e) {
    System.debug(e.getMessage());
}   
}