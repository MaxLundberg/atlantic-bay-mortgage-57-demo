trigger updateOppyStage on Opportunity (before insert, before update) {

    for (Opportunity so : Trigger.new) {
        //friends remind friends to bulkify
        if(so.StageName.contains( 'Canc/Withdn')){
            so.StageName = 'Closed Lost';
            so.Jungo_LenderApp__Closed_Reason__c = 'Canc/Withdrawn';
        }
        else if (so.StageName.contains('Canc/Withdrawn')){
            so.STageName = 'Closed Lost';
            so.Jungo_LenderApp__Closed_Reason__c = 'Canc/Withdrawn';
        }
        else if (so.StageName.contains('W/D for Incompleteness')) {
            so.StageName = 'Closed Lost';
            so.Jungo_LenderApp__Closed_Reason__c = 'W/D for Incompleteness';
        }
        else if (so.StageName.contains('Audited by Compliance')){
            so.StageName = 'Closed Won';
            so.Jungo_LenderApp__Closed_Reason__c = 'Audited by Compliance';
        }
        else if (so.StageName.contains('Funded (ABMG)')) {
            so.StageName = 'Closed Won';
            so.Jungo_LenderApp__Closed_Reason__c = 'Funded(ABMG)';
        }
        else if (so.StageName.contains('Funded by Corr')){
            so.StageName = 'Closed Won';
            so.Jungo_LenderApp__Closed_Reason__c = 'Funded by Corr';
        }
        else if (so.StageName.contains('Loan Sold')){
            so.StageName = 'Closed Won';
            so.Jungo_LenderApp__Closed_Reason__c = 'Loan Sold';
        }
        else if (so.StageName.contains('Paid in Full')){
            so.StageName = 'Closed Won';
            so.Jungo_LenderApp__Closed_Reason__c = 'Paid in Full';
        }
        else if (so.StageName.contains('Purchased')){
            so.StageName = 'Closed Won';
            so.Jungo_LenderApp__Closed_Reason__c = 'Purchased';
        }
        else if (so.StageName.contains('Purchased from Corr')){
            so.StageName = 'Closed Won';
            so.Jungo_LenderApp__Closed_Reason__c = 'Purchased';
        }
        else if (so.StageName.contains('Credit Only (Denied/WD)')){
            so.StageName = 'Closed Lost';
            so.Jungo_LenderApp__Closed_Reason__c = 'Credit Only (Denied/WD)';
        }
        else if (so.StageName.contains('Denied')){
            so.StageName = 'Closed Lost';
            so.Jungo_LenderApp__Closed_Reason__c = 'Denied';
        }
    }

}