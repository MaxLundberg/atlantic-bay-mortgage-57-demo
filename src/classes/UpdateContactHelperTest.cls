/*********************************************************************
*
*   File Name: UpdateContactHelperTest.cls
*
*   File Description: Test class for UpdateContactHelper
*
**********************************************************************
*
*   Date             Author               Change
*   08/17/15         Rob                  Initial creation                        
*
*********************************************************************/
@isTest
public class UpdateContactHelperTest
{
    static testMethod void contactTests()
    {
        Account acc = new Account();
        acc.Name = 'database';
        insert acc;
        Contact con = new Contact(LastName = 'test', Jungo_LenderApp__Loan_Officer__c = 'admin', Jungo_LenderApp__Stage__c = 'Purchased from Corr', Jungo_LenderApp__Group__c = 'Appraiser', AccountId = acc.Id, LeadSource = 'Broker');
        insert con;
        Contact c = [select AccountId from Contact where Id =: con.Id];
        system.assertEquals(acc.Id, c.AccountId);

        Account acc1 = new Account();
        acc1.Name = 'Juliana Gulitz database';
        insert acc1;
        Contact con1 = new Contact(LastName = 'test', Jungo_LenderApp__Loan_Officer__c = 'admin', Jungo_LenderApp__Group__c = 'Appraiser', AccountId = acc.Id, LeadSource = 'Broker');
        insert con1;
        Contact c1 = [select AccountId from Contact where Id =: con1.Id];
        system.assertNotEquals(acc1.Id, c1.AccountId);

    }
    

}