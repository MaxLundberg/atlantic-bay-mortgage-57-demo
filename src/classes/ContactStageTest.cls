@isTest 
public class ContactStageTest {
    static testmethod void insertNewContactTest() {
        Test.startTest();
        Contact con = new Contact();
    con.FirstName = 'test';
    con.Lastname = 'test1';
        con.Jungo_LenderApp__Group__c = 'Appraiser';
        con.Jungo_LenderApp__Stage__c = 'Purchased from Corr';
            insert con;
        System.assertEquals(con.Jungo_LenderApp__Stage__c, 'Purchased from Corr');
        con.Jungo_LenderApp__Stage__c = 'Approved with Conditions';
        	update con;
        System.assertEquals(con.Jungo_LenderApp__Stage__c, 'Approved with Conditions');
        List<Contact> searchList = [SELECT id, Jungo_LenderApp__Stage__c FROM contact Where Id =: con.Id];
        for(Contact c : searchList){
            system.assertNotEquals(c.Jungo_LenderApp__Stage__c, 'Loan in Process');
        }
}
}