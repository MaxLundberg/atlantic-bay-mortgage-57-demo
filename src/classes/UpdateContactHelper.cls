public class UpdateContactHelper 
{
    public static void updateContact(List<Contact> triggerNew)
    {
        // retrieve the accounts and owners
        
        Set<string> UserIds = new Set<string>();
        for(Contact c1 : triggerNew)
        {
            UserIds.add(c1.Jungo_LenderApp__Loan_Officer__c);
        }
        Map<Id, String> ownerIdName = new Map<Id, String>();

        //owners
        Map<string, User> owners = new Map<string, User>();
        Set<String> ownerNames = new Set<String>();
        for (User u : [Select JungoBteSync__Byte_User_ID__c, Name, Id  from User])
        {
            owners.put(u.JungoBteSync__Byte_User_ID__c,u);
            ownerNames.add(returnLoanOfficerName(u.Name));
            ownerIdName.put(u.Id, u.Name);

        }
        
        //create account maps
        List<Account> allAccounts = [select Id, Name from Account where Name = 'database' OR Name in:ownerNames];
        Map<String, String> accountNameToAccountId = new Map<String, String>();   

        for(Account a: allAccounts)
        {
            System.debug( LoggingLevel.DEBUG, '=> ' + a );
            String name = a.Name;
            accountNameToAccountId.put(name.toLowerCase(), a.Id);
        }

        for(Contact con : triggerNew)
        {
        if(con.Historical_Data__c == false & con.accountid == NULL){     
            String usr = returnLoanOfficerName(OwnerIdName.get(con.OwnerId));    

            String accountIdToAssociate = null;
      System.debug('=> Back from the future usr: ' + usr.toLowerCase());
            if(accountNameToAccountId.containsKey(usr.toLowerCase()))
            {
                accountIdToAssociate = accountNameToAccountId.get(usr.toLowerCase());
            }
            else //if(accountNameToAccountId.containsKey('database'))
            { 
                accountIdToAssociate = accountNameToAccountId.get('database');
            }

            if(accountIdToAssociate != null)
            {
                System.debug('=> accountIdToAssociate ' + accountIdToAssociate );
                con.AccountId = accountIdToAssociate;
            }
            }
        }
    }

    private static String returnLoanOfficerName(String ownerName)
    {        
        Set<String> loanOfficerNames = new Set<String>();
        string nametosplit = ownerName;
        system.debug('my name is : ' + nametosplit);
        if(String.isNotBlank(nametosplit))
        {
            
            String[] splitName = nametosplit.split(' ');
            if( splitName.size() == 2)
            {
                loanOfficerNames.add(splitName[0] + ' ' + splitName[1]);
            } 
           system.debug(splitName);
        }
        
        String loanOfficer;
        for(String lo : loanOfficerNames)
        {
            loanOfficer = lo;
        }
        
        String usr = loanOfficer + ' ' + 'database';
        System.debug( LoggingLevel.DEBUG, '=> user ' + loanOfficer );
        System.debug( LoggingLevel.DEBUG, '=> usr ' + usr );

        return usr;
    }
}