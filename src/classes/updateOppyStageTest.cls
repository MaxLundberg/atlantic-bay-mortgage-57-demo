@isTest 
public class updateOppyStageTest {
    static testmethod void insertNewContactTest() {
        Test.startTest();
        Opportunity con = new Opportunity();
    con.Name = 'test';
        con.CloseDate = Date.today();
        con.StageName = 'Denied';
            insert con;
        System.assertEquals(con.StageName, 'Denied');
        System.assertNotEquals(con.Jungo_LenderApp__Closed_Reason__c,  'Denied');
        
        con.StageName = 'Purchased from Corr';
        update con;

        List<Opportunity> searchList = [SELECT id, StageName, Jungo_LenderApp__Closed_Reason__c FROM opportunity Where Id =: con.Id];
        for(Opportunity c : searchList){
            system.assertEquals(c.StageName, 'Closed Won');
            system.assertEquals(c.Jungo_LenderApp__Closed_Reason__c, 'Purchased');
        }
        con.StageName = 'Canc/Withdn';
        update con;

        List<Opportunity> searchList1 = [SELECT id, StageName, Jungo_LenderApp__Closed_Reason__c FROM opportunity Where Id =: con.Id];
        for(Opportunity c : searchList1){
            system.assertEquals(c.StageName, 'Closed Lost');
            system.assertNotEquals(c.Jungo_LenderApp__Closed_Reason__c, 'Purchased');
        }
con.StageName = 'Canc/Withdrawn';
        update con;

        List<Opportunity> searchList2 = [SELECT id, StageName, Jungo_LenderApp__Closed_Reason__c FROM opportunity Where Id =: con.Id];
        for(Opportunity c : searchList2){
            system.assertEquals(c.StageName, 'Closed Lost');
            system.assertNotEquals(c.Jungo_LenderApp__Closed_Reason__c, 'Purchased');
        }
con.StageName = 'W/D for Incompleteness';
        update con;

        List<Opportunity> searchList3 = [SELECT id, StageName, Jungo_LenderApp__Closed_Reason__c FROM opportunity Where Id =: con.Id];
        for(Opportunity c : searchList3){
            system.assertEquals(c.StageName, 'Closed Lost');
            system.assertNotEquals(c.Jungo_LenderApp__Closed_Reason__c, 'Purchased');
        }
        con.StageName = 'Audited by Compliance';
        update con;

        List<Opportunity> searchList4 = [SELECT id, StageName, Jungo_LenderApp__Closed_Reason__c FROM opportunity Where Id =: con.Id];
        for(Opportunity c : searchList4){
            system.assertEquals(c.StageName, 'Closed Won');
            system.assertNotEquals(c.Jungo_LenderApp__Closed_Reason__c, 'Purchased');
        }
con.StageName = 'Funded (ABMG)';
        update con;

        List<Opportunity> searchList5 = [SELECT id, StageName, Jungo_LenderApp__Closed_Reason__c FROM opportunity Where Id =: con.Id];
        for(Opportunity c : searchList5){
            system.assertEquals(c.StageName, 'Closed Won');
            system.assertNotEquals(c.Jungo_LenderApp__Closed_Reason__c, 'Purchased');
        }
con.StageName = 'Funded by Corr';
        update con;

        List<Opportunity> searchList6 = [SELECT id, StageName, Jungo_LenderApp__Closed_Reason__c FROM opportunity Where Id =: con.Id];
        for(Opportunity c : searchList6){
            system.assertEquals(c.StageName, 'Closed Won');
            system.assertNotEquals(c.Jungo_LenderApp__Closed_Reason__c, 'Purchased');
        }
con.StageName = 'Loan Sold';
        update con;

        List<Opportunity> searchList7 = [SELECT id, StageName, Jungo_LenderApp__Closed_Reason__c FROM opportunity Where Id =: con.Id];
        for(Opportunity c : searchList7){
            system.assertEquals(c.StageName, 'Closed Won');
            system.assertNotEquals(c.Jungo_LenderApp__Closed_Reason__c, 'Purchased');
        }


}
}